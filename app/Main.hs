{-# LANGUAGE RecordWildCards, OverloadedStrings, DataKinds, ScopedTypeVariables #-}
{-# LANGUAGE ApplicativeDo #-}
module Main (module Main) where

import Interlude
import InternatMustache
import Options.Applicative
import System.Directory
import Control.Effects.Early
import System.FilePath
import Control.Effects.Signal
import qualified Text.Parsec.Error as Parsec

data CmdOptions = CmdOptions
    { languageFilePath :: FilePath
    , templateDirectoryInputPath :: FilePath
    , outputDirectoryPath :: FilePath
    , delimiter :: Delimiter }

cmdOptionsParser :: Parser CmdOptions
cmdOptionsParser = CmdOptions
    <$> strOption
        (  long "lang" <> metavar "CSV_FILE"
        <> help "Path to the input CSV file with translations" )
    <*> strOption
        (  long "in" <> metavar "TEMPLATE_DIRECTORY"
        <> help "Path to directory containing template files to translate" )
    <*> strOption
        (  long "out" <> metavar "OUTPUT_DIRECTORY"
        <> help "Path to the output directory" )
    <*> (   Delimiter
        <$> strOption
            (  long "ldel" <> metavar "LEFT_DELIMITER"
            <> value "{{{{" <> showDefault )
        <*> strOption
            (  long "rdel" <> metavar "RIGHT_DELIMITER"
            <> value "}}}}" <> showDefault ) )

partitionA :: Applicative m => (a -> m Bool) -> [a] -> m ([a], [a])
partitionA p (x : xs) = do
    b <- p x
    (yes, no) <- partitionA p xs
    pure $ if b then (x : yes, no) else (yes, x : no)
partitionA _ [] = pure ([], [])

-- | Lists all the files in a directory and all it's subdirectories
listDirRecursive :: MonadIO m => FilePath -> m [FilePath]
listDirRecursive fp = do
    fps <- fmap (fp </>) <$> liftIO (listDirectory fp)
    (files, dirs) <- partitionA (liftIO . doesFileExist) fps
    sub <- mapM listDirRecursive dirs
    return (files <> concat sub)

main :: IO ()
main = do
    CmdOptions{..} <- execParser (
        info (cmdOptionsParser <**> helper)
             (  fullDesc
             <> progDesc "Translate a batch of templates using the given translation file" ) )
    handleEffects $ do
        langData <- parseLanguageFile languageFilePath
        entries <- listDirRecursive templateDirectoryInputPath
        forM_ entries $ \e -> handleEarly $ do
            putStrLn $ "Translating " <> e
            templ <- parseTemplate delimiter e
            translations <- translateTemplate langData templ
            writeTranslations
                outputDirectoryPath (makeRelative templateDirectoryInputPath e) 
                translations
    where
    handleEffects =
          handleException (\(Error fp :: Error "fileNotFound") -> error $ "File not found: " <> toS fp)
        . handleException (\(p :: Parsec.ParseError) -> error (show p))
        . handleException (\(TemplateError _ subs) -> error (show subs))
        . handleFSIO
