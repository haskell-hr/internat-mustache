# internat-mustache
Command line utility for translating projects using the internat library.
Given an input directory and a translation file, it recursively translates every file and for
each language it creates an identical file structure in the output directory.

To insert a piece of translatable text into your file use this syntax: `{{{{ lang.SomeStringIdentifier }}}}`
(replace `SomeStringIdentifier` with some key from your translation file)

```man
Usage: internat-mustache-exe.exe --lang CSV_FILE --in TEMPLATE_DIRECTORY
                                 --out OUTPUT_DIRECTORY [--ldel LEFT_DELIMITER]
                                 [--rdel RIGHT_DELIMITER]
  Translate a batch of templates using the given translation file
```
