{-# LANGUAGE TypeFamilies, TypeApplications, FlexibleContexts, MultiParamTypeClasses, DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
module InternatMustache
    ( module InternatMustache
    ) where

import Interlude
import Internat
import Text.Mustache as Mustache
import Text.Mustache.Parser as Mustache
import Control.Effects.Signal
import qualified Data.ByteString as BS
import GHC.TypeLits
import Text.Parsec.Error
import qualified Data.Map as Map
import System.Directory
import Text.Mustache.Render
import Text.Mustache.Types
import System.FilePath

newtype TranslationData = TranslationData (Map (Language, Text) Text)
    deriving (Eq, Ord, Read, Show)

data Delimiter = Delimiter Text Text

newtype Language = Language Text deriving (Eq, Ord, Read, Show)

data TemplateError = TemplateError Template [SubstitutionError]

data FileRead
type instance EffectMsg FileRead = FilePath
type instance EffectRes FileRead = ByteString
readFileEff :: MonadEffect FileRead m => FilePath -> m ByteString
readFileEff = effect (Proxy @FileRead)

data FileWrite
type instance EffectMsg FileWrite = (FilePath, ByteString)
type instance EffectRes FileWrite = ()
writeFileEff :: MonadEffect FileWrite m => FilePath -> ByteString -> m ()
writeFileEff = curry (effect (Proxy @FileWrite))

handleFSIO ::
    (Throws (Error "fileNotFound") m, MonadIO m)
    => EffectHandler FileWrite (EffectHandler FileRead m) a -> m a
handleFSIO m = handleEffect r (handleEffect w m)
    where
    w (fp, bs) = do
        ex <- liftIO (doesFileExist fp)
        unless ex $ liftIO $
            createDirectoryIfMissing True (takeDirectory fp)
        liftIO (BS.writeFile fp bs)
    r fp = do
        ex <- liftIO (doesFileExist fp)
        unless ex $ throwSignal $ Error @"fileNotFound" (toS fp)
        liftIO (BS.readFile fp)



newtype Error (errType :: Symbol) = Error Text deriving (Eq, Ord, Read, Show)

-- @TODO: Get rid of MonadIO by exposing more functions in internat that handle already read files

parseLanguageFile ::
    ( MonadEffect FileRead m, Throws (Error "fileNotFound") m
    , MonadIO m )
    => FilePath -> m TranslationData
parseLanguageFile fp = do
    ex <- liftIO (doesFileExist fp)
    unless ex $ throwSignal $ Error @"fileNotFound" ("File " <> toS fp <> " does not exist")
    m <- extractMapFromCsvFile (toS fp)
    return (TranslationData (Map.mapKeys (first Language) m))

parseTemplate ::
    (MonadEffect FileRead m, Throws ParseError m)
    => Delimiter -> FilePath -> m Template
parseTemplate (Delimiter del1 del2) path = do
    file <- readFileEff path
    let parseResult = Mustache.parseWithConf (MustacheConf (toS del1, toS del2)) path (toSL file)
    either throwSignal streeToTemplate parseResult
    where
    streeToTemplate t = return (Template path t mempty)

translateTemplateTo ::
    (MonadEffectSignal TemplateError Text m)
    => [Pair] -> Language -> TranslationData -> Template -> m Text
translateTemplateTo ctx lang (TranslationData m) t =
    if null errs then return res else signal (TemplateError t errs)
    where
    transValue =
        Map.toList m
        & mapMaybe (\((lang', term), tran) -> guard (lang' == lang) >> Just (term, toMustache tran))
        & object
    (errs, res) = Mustache.checkedSubstitute t (object (("lang" ~> transValue) : ctx))

translateTemplate ::
    (MonadEffectSignal TemplateError Text m)
    => TranslationData -> Template -> m [(Language, Text)]
translateTemplate td@(TranslationData m) t = do
    translations <- mapM (\l -> translateTemplateTo [allFalseExcept l] l td t) languages
    return (zip languages translations)
    where
    languages = fst <$> Map.keys m
    allFalseExcept l = "activeLang" ~> object (do
        l'@(Language langName) <- languages
        return (langName ~> toMustache (l == l')))

writeTranslations ::
    (MonadEffect FileWrite m)
    => FilePath -> FilePath -> [(Language, Text)] -> m ()
writeTranslations dirPath templPath translations =
    forM_ translations $ \(Language lang, t) ->
        writeFileEff (dirPath </> toS lang </> templPath) (toSL t)
